


#include "stdafx.h"

#include <iostream>

/*
inSizeX, inSizeY : taille de l'image � cr�er
inTabPoints : tableau contenant la liste des points � afficher
*/
int displayImage(int inSizeX, int inSizeY, std::vector<Point> inTabPoints) {

	Mat image; // cr�ation d'une image unsigned char
	Mat imageZoom;

	int pixVal = 255;

	namedWindow("Display window2", WINDOW_AUTOSIZE); // Create a window for display.

	// Pour chaque point
	int i = 0;
	for (std::vector<Point>::iterator it = inTabPoints.begin(); it != inTabPoints.end(); it++ ) {
		
		// On remet l'image � 0
		image = Mat::zeros(inSizeY, inSizeX, CV_8UC(1));
		
		// On affiche en blanc le point courant
		image.row(it->y).col(it->x) = 255;

		resize(image, imageZoom, Size(), 5, 5, 0);

		// On affichage l'image r�sultante
		imshow("Display window2", imageZoom); // Show our image inside it.
		waitKey(0); // On attend que l'utilisateur appuie sur une touche

		i++;
	}

	return 0;
}
