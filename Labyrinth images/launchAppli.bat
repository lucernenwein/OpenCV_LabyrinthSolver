@echo OFF

REM Path to exe
set exePath="C:\Users\Luc\Documents\Visual Studio 2015\Projects\TestOpenCV\x64\Release\TestOpenCV.exe"

REM Parameters
set imagePath="C:\Users\Luc\Documents\Visual Studio 2015\Projects\TestOpenCV\Test files\labyrinthe5.png"
set speedProcessDisplay=500
set speedPathDisplay=70
set resultImagePath="C:\\Users\\Luc\\Documents\\Visual Studio 2015\\Projects\\TestOpenCV\\Test files\\result.png"

REM Launching application
%exePath% %imagePath% %speedProcessDisplay% %speedPathDisplay% %resultImagePath%