#include "stdafx.h"


#include "SP_Pixel.h"


SP_Pixel::SP_Pixel()
{
}


SP_Pixel::~SP_Pixel()
{
}

SP_Pixel::SP_Pixel(const int x, const int y, const double dist, SP_Pixel *previousPix)
	: pt(cv::Point(x, y)), dist(dist), previousPix(previousPix) {}


SP_Pixel::SP_Pixel(const int x, const int y, SP_Pixel *previousPix)
	: pt(cv::Point(x, y)), dist(0), previousPix(previousPix) {}


SP_Pixel::SP_Pixel(const int x, const int y)
	: pt(cv::Point(x, y)), dist(0), previousPix(nullptr) {}


SP_Pixel::SP_Pixel(const cv::Point pt, const double dist, SP_Pixel *previousPix)
	: pt(pt), dist(dist), previousPix(previousPix) {}


SP_Pixel::SP_Pixel(const cv::Point pt, const double dist)
	: pt(pt), dist(dist), previousPix(nullptr) {}


SP_Pixel::SP_Pixel(const cv::Point pt, SP_Pixel *previousPix)
	: pt(pt), dist(0), previousPix(previousPix) {}


SP_Pixel::SP_Pixel(const cv::Point pt)
	: pt(pt), dist(0), previousPix(nullptr) {}


const bool SP_Pixel::operator<(const SP_Pixel & SP_pt1) const {
	return dist < SP_pt1.dist ? true : false;
}

