# Labyrinth image solver

This is a small program I made that is designed to solve labyrinth images. To be usable, the input image must respect following conventions:
1. pixels with a value > 128 are walkable,
2. pixels with a value <= 128 are walls,
3. The starting pixel is blue,
4. The ending pixel is red.

It is possible to display the algorithm's advancement in realtime in a window, or letting the algorithm get to the end before showing the result. This is useful because it's actually the draw calls that take most of the time.

In this project I'm using OpenCV just as a mean of loading and saving images, getting pixel values and displaying the result in a window in realtime.

## Algorithm principle

The algorithm I used is very inspired by classic propagation algorithms like watershed. The principle is pretty easy, as I'm using a binary heap to store pixels and basically letting it do the job! More precisely, here's the pseudocode:
1. I add the starting pixel to the heap (weight value associated is minus the distance to the end),
2. I pull the pixel on top of the heap (the one closest to the end),
3. I look for walkable pixels in a 4-neighbourhood around this pixel,
4. I add these walkable pixels in the heap and color them green in the image,
5. I write down in a pointer map that these neighbour pixels are sons of the pulled pixel,
6. Go back to 2., repeat until I pull the end pixel from the heap,
7. Finally, I just have to unroll the linked list starting from the end point to get the solution path.

## Results

Here is a list of some results I got on a variety of labyrinths I found on internet. The largest labyrinth I could test my program on has 5000*5000 passages on a 10001*10001 map, with a total of 2.534.438 dead ends (see http://www.astrolog.org/labyrnth/maze.htm). This labyrinth took around 10s to complete, which is not too shaby for such a simple algorithm! The result is shown in figure 6 below.

Actually I wanted to try an even bigger image, but OpenCV failed on me as I reached its capability of image size it could load... sad!

![](Labyrinth images/labyrinth10_result.png)*Figure 1: Circular labyrinth*

![](Labyrinth images/labyrinth15_result.png)*Figure 2: Parallelepipedic labyrinth with circular patterns*

![](Labyrinth images/labyrinth17_result.png)*Figure 3: Labyrinth with mostly straight paths (this one would be annoying to do by hand, as it seem to "flicker"!)*

![](Labyrinth images/labyrinth18_result.png)*Figure 4: Hand-drawn labyrinth*

![](Labyrinth images/labyrinth20_result.png)*Figure 5: Labyrinth with circular patterns and mostly empty space*

![](Labyrinth images/labyrinth22_result.png)*Figure 6: Big'ol labyrinth: don't try to do this one by hand*
