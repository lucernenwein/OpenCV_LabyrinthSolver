#pragma once

#include "stdafx.h"

class SP_Pixel
{

public:
	cv::Point pt;
	double dist;
	SP_Pixel *previousPix;

	SP_Pixel();
	SP_Pixel(const int x, const int y, const double dist, SP_Pixel *previousPix);
	SP_Pixel(const int x, const int y);
	SP_Pixel(const int x, const int y, SP_Pixel *previousPix);
	SP_Pixel(const cv::Point pt, const double dist, SP_Pixel *previousPix);
	SP_Pixel(const cv::Point pt, SP_Pixel *previousPix);
	SP_Pixel(const cv::Point pt, const double dist);
	SP_Pixel(const cv::Point pt);
	~SP_Pixel();

	const bool operator<(const SP_Pixel &SP_pt1) const;
};

