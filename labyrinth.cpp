
#include "stdafx.h"
#include "labyrinth.h"
#include "testAffichage.h"

// Bleu vers rouge
#define ColorBegin Vec3b(255, 0, 0)
#define ColorEnd Vec3b(0, 0, 255)
#define ColorProcessed Vec3b(120, 255, 120)
#define ColorPath Vec3b(80, 80, 255)


// Weight function used to order the heap containing the pixels to process.
double get_SP_distance(Point& pt1, Point& pt2) {
	return - sqrt((pt1.x - pt2.x)*(pt1.x - pt2.x) + (pt1.y - pt2.y)*(pt1.y - pt2.y));
}


/* Function that looks for the 2 first points with values ColorBegin and ColorEnd in inImg.*/
int getBeginEnd(Mat& inImg, Point& outPtBegin, Point& outPtEnd) {

	bool foundBegin = false, foundEnd = false;

	// Loop on all pixels
	for (int x = 0; x < inImg.cols; x++) {
		for (int y = 0; y < inImg.rows; y++) {

			Vec3b curPix = inImg.at<Vec3b>(y, x);

			// We found beginning pixel
			if (inImg.at<Vec3b>(y, x) == ColorBegin) {
				outPtBegin = Point(x, y);
				foundBegin = true;
			}
			// We found end pixel
			else if(inImg.at<Vec3b>(y, x) == ColorEnd) {
				outPtEnd = Point(x, y);
				foundEnd = true;
			}
		}
	}

	if (foundBegin && foundEnd) {
		return EXIT_SUCCESS;
	}
	else {
		return EXIT_FAILURE;
	}
}


/*
This function finds the path between two input point in a labyrinth image.
inImg: labyrinth image (color image)
inPtBegin: begin point
inPtEnd: end point
inSpeed: number of pixels to process before refreshing the display (higher is faster)
outTabPts: vector containing points from end to beginning
outImg: result image
*/
int getShortestPath(Mat& inImg, Point& inPtBegin, Point& inPtEnd, int inSpeedProcess, int inSpeedPath, std::vector<Point>& outTabPts, Mat& outImg) {

	// Creating a copy of inImg in which we mark already processed pixels
	inImg.copyTo(outImg);

	long counter = 0;

	priority_queue<SP_Pixel> pixHeap;

	// Initialization of heap with beginning point
	SP_Pixel firstPt = SP_Pixel(inPtBegin, get_SP_distance(inPtBegin, inPtEnd));
	pixHeap.push(firstPt);

	// Table representing all points in the image
	std::vector<SP_Pixel> tabPix(inImg.cols * inImg.rows, SP_Pixel(0, 0, nullptr));
	tabPix[firstPt.pt.y * inImg.cols + firstPt.pt.x] = firstPt;

	// As long as the heap contains a point
	while (!pixHeap.empty()) {

		// Getting pixel on top and removing it
		SP_Pixel curPix = pixHeap.top();
		pixHeap.pop();
		
		// Checking if current point is the end
		if (curPix.pt.x == inPtEnd.x && curPix.pt.y == inPtEnd.y) {
			getLabyrinthPath(tabPix, inPtEnd, inImg, outTabPts);
			printPathInImage(outTabPts, outImg, ColorPath, inSpeedPath);
			return EXIT_SUCCESS;
		}

		// This step is only for optimization purposes (can be removed without modifying
		// anything else). If current pixel only has only one possible neighbour, we unroll 
		// the path until we get to the next intersection (this allows not using the heap 
		// for simple paths).
		SP_Pixel neighNext;
		while (getOnlyPossibleNeighbour(curPix, inImg, outImg, neighNext)) {

			// chaining neighbour to previous pixel
			neighNext.previousPix = &tabPix[curPix.pt.y * inImg.cols + curPix.pt.x];
			tabPix[neighNext.pt.y * inImg.cols + neighNext.pt.x] = neighNext;

			// indicate the pixel as processed
			outImg.row(neighNext.pt.y).col(neighNext.pt.x) = ColorProcessed;

			curPix = neighNext;
		}

		// Getting neighbour pixels
		SP_Pixel neighLeft, neighUp, neighRight, neighDown;
		neighLeft = SP_Pixel(curPix.pt.x - 1, curPix.pt.y);
		neighUp = SP_Pixel(curPix.pt.x, curPix.pt.y - 1);
		neighRight = SP_Pixel(curPix.pt.x + 1, curPix.pt.y);
		neighDown = SP_Pixel(curPix.pt.x, curPix.pt.y + 1);

		// Adding neighbouring pixels to heap (if conditions are satisfied)
		addNeighbourPixel(neighDown, curPix, inPtEnd, pixHeap, inImg, outImg, tabPix);
		addNeighbourPixel(neighUp, curPix, inPtEnd, pixHeap, inImg, outImg, tabPix);
		addNeighbourPixel(neighRight, curPix, inPtEnd, pixHeap, inImg, outImg, tabPix);
		addNeighbourPixel(neighLeft, curPix, inPtEnd, pixHeap, inImg, outImg, tabPix);
		
		// Every inSpeed points, we display the processed pixels image
		if (inSpeedProcess > 0) {
			if ((counter = counter % inSpeedProcess) == 0) {
				imshow("Display window2", outImg);
				waitKey(1); // waiting at least 1ms (else nothing is displayed)
			}
			counter++;
		}
	}

	return EXIT_FAILURE; // We didn't find the exit
}



/*
This function takes a neighbour pixel, checks if it has been processed. If no:
Computes distance of neighbour pixel to the end, links it to previous pixel then adds 
it to the heap.
inNeighPix: neighbour pixel to test
inCurPix: pixel just before inNeighPix
inPtEnd: end pixel of labyrinth (used to compute distance to inNeighPix)
inPixHeap: heap containing all pixels to test
inImg: input image (labyrinth)
inImgProcessed: labyrinth + processed pixels marked
*/
int addNeighbourPixel(SP_Pixel& inNeighPix, SP_Pixel& inCurPix, Point& inPtEnd, 
					priority_queue<SP_Pixel>& inPixHeap, Mat& inImg, Mat& inImgProcessed, 
					std::vector<SP_Pixel>& inOutTabPts) {

	if (inNeighPix.pt.x >= 0 && inNeighPix.pt.y >= 0 &&
		inNeighPix.pt.y < inImg.rows && inNeighPix.pt.x < inImg.cols) {
		// Pixel is in the image

		Vec3b curVal = inImg.at<Vec3b>(inNeighPix.pt.y, inNeighPix.pt.x); // getting RGB value

		if (((curVal.val[0] > 128 && curVal.val[1] > 128 && curVal.val[2] > 128)
			|| curVal == ColorEnd)
			&& inImgProcessed.at<Vec3b>(inNeighPix.pt.y, inNeighPix.pt.x) != ColorProcessed) {
			// pixel is not a wall or the end, and has not been processed

			// adding pixel to heap
			inPixHeap.push(SP_Pixel(inNeighPix.pt, get_SP_distance(inNeighPix.pt, inPtEnd)));

			// chaining neighbour to previous pixel
			inNeighPix.previousPix = &inOutTabPts[inCurPix.pt.y * inImg.cols + inCurPix.pt.x];
			inOutTabPts[inNeighPix.pt.y * inImg.cols + inNeighPix.pt.x] = inNeighPix;

			// indicate the pixel as processed
			inImgProcessed.row(inNeighPix.pt.y).col(inNeighPix.pt.x) = ColorProcessed; 

			return EXIT_SUCCESS;
		}
	}

	return EXIT_FAILURE;
}


/* This function unrolls the path starting from the end, by following the SP_Pixel previousPoint linked list.
inTabPts: table representing all pixels in the image
inPtEnd: end point of labyrinth
inImg: image of labyrinth
outTabPts: table of points representing the path in the labyrinth
*/
int getLabyrinthPath(std::vector<SP_Pixel>& inTabPts, Point& inPtEnd, Mat& inImg, std::vector<Point>& outTabPts) {
	
	if (inTabPts.empty()) {
		return EXIT_FAILURE;
	}

	// Getting end point
	SP_Pixel curPt = inTabPts[inPtEnd.y * inImg.cols + inPtEnd.x];

	long counter = 0;

	// Following linked list to begin point
	while (curPt.previousPix != nullptr) {
		outTabPts.push_back(curPt.pt);
		curPt = *(curPt.previousPix);
	}

	return EXIT_SUCCESS;
}


/*
This function displays points from a table in an image.
inTabPts: table of points
inImg: image in which to draw points from the table
inColor: color of the points
*/
int printPathInImage(std::vector<Point>& inTabPts, Mat& inImg, Vec3b inColor, int inSpeed) {

	long counter = 0;
	for (std::vector<Point>::iterator i = inTabPts.begin(); i != inTabPts.end(); i++) {

		inImg.at<Vec3b>(i->y, i->x) = inColor;
		if (inSpeed > 0) {
			if ((counter = counter % inSpeed) == 0) {
				imshow("Display window2", inImg);
				waitKey(1); // waiting at least 1ms (else nothing is displayed)
			}
			counter++;
		}
	}
	return EXIT_SUCCESS;
}


/*
Returns true if inNeighPix is walkable, false otherwise.
This function is meant to be used in the path unrolling optimization step.
N.B.: end point is not considered walkable, as it has to go through the heap.
inPtEnd : end pixel of labyrinth(used to compute distance to inNeighPix)
inImg : input image(labyrinth)
inImgProcessed : labyrinth + processed pixels marked
*/
bool isNeighbourOK(SP_Pixel& inNeighPix, Mat& inImg, Mat& inImgProcessed) {

	if (inNeighPix.pt.x >= 0 && inNeighPix.pt.y >= 0 &&
		inNeighPix.pt.y < inImg.rows && inNeighPix.pt.x < inImg.cols) {
		// Pixel is in the image

		Vec3b curVal = inImg.at<Vec3b>(inNeighPix.pt.y, inNeighPix.pt.x); // getting RGB value

		if ((curVal.val[0] > 128 && curVal.val[1] > 128 && curVal.val[2] > 128)
			&& inImgProcessed.at<Vec3b>(inNeighPix.pt.y, inNeighPix.pt.x) != ColorProcessed
			&& inImgProcessed.at<Vec3b>(inNeighPix.pt.y, inNeighPix.pt.x) != ColorEnd) {
			// pixel is not a wall, not the end, and has not been processed

			return true;
		}
	}

	return false;
}


/*
This function checks walkability of all 4 neighbours of inCurPix. If only one is walkable,
returns true and outNeighbour is this neighbour. Returns false otherwise.
This function is meant to be used in the path unrolling step.
inCurPix : pixel just before inNeighPix
inPtEnd : end pixel of labyrinth(used to compute distance to inNeighPix)
inImg : input image(labyrinth)
inImgProcessed : labyrinth + processed pixels marked
outNeighbour : only possible neighbour of inCurPix
*/
bool getOnlyPossibleNeighbour(SP_Pixel& inCurPix, Mat& inImg,
	Mat& inImgProcessed, SP_Pixel& outNeighbour) {

	// Getting neighbour pixels
	SP_Pixel neighLeft, neighUp, neighRight, neighDown;
	neighLeft = SP_Pixel(inCurPix.pt.x - 1, inCurPix.pt.y);
	neighUp = SP_Pixel(inCurPix.pt.x, inCurPix.pt.y - 1);
	neighRight = SP_Pixel(inCurPix.pt.x + 1, inCurPix.pt.y);
	neighDown = SP_Pixel(inCurPix.pt.x, inCurPix.pt.y + 1);

	int nbNeighbourOK = 0;

	// Getting the number of neighbours that are walkable
	if (isNeighbourOK(neighLeft, inImg, inImgProcessed)) {
		nbNeighbourOK++;
		outNeighbour = neighLeft;
	}
	if (isNeighbourOK(neighUp, inImg, inImgProcessed)) {
		nbNeighbourOK++;
		outNeighbour = neighUp;
	}
	if (isNeighbourOK(neighRight, inImg, inImgProcessed)) {
		nbNeighbourOK++;
		outNeighbour = neighRight;
	}
	if (isNeighbourOK(neighDown, inImg, inImgProcessed)) {
		nbNeighbourOK++;
		outNeighbour = neighDown;
	}

	// Only one neighbour
	if (nbNeighbourOK == 1) {
		return true;
	}

	// No or more than one neighbour
	return false;
}
