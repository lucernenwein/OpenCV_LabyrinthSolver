// TestOpenCV.cpp�: d�finit le point d'entr�e pour l'application console.
//

#include "stdafx.h"
#include "testAffichage.h"
#include "labyrinth.h"


int main(int argc, char** argv) {

	// Checking input parameters
	if (argc != 5) {
		cout << "Usage: testOpenCV.exe <imagePath> <speedProcess> <speedPath> <resultImagePath>" << endl;
		cout << "\timagePath: path to the labyrinth image" << endl;
		cout << "\tspeedProcess: number of pixels to process before refreshing display (higher=faster, 0=display only at the end)" << endl;
		cout << "\tspeedPath: number of pixels to draw following the final path before refreshing display (higher=faster 0=display only at the end)" << endl;
		cout << "\resultImagePath: path where to save the result image (""=nothing saved on disk)" << endl;
		return EXIT_FAILURE;
	}

	Mat imgLabyrinth, imgLabyrinthZoom, imgResult;

	// Getting command line parameters
	string inputImgPath = argv[1];
	int speedProcess = stoi(argv[2], nullptr, 10);
	int speedPath = stoi(argv[3], nullptr, 10);

	// Loading input image
	clock_t initTime = clock();

	imgLabyrinth = imread(inputImgPath, IMREAD_COLOR); // Read the file IMREAD_GRAYSCALE

	if (!imgLabyrinth.data) { // Check for invalid input
		cout << "Could not open or find image: " << inputImgPath << "." << std::endl;
		return EXIT_FAILURE;
	}

	cout << "Loading time: " << clock() - initTime << " ms" << endl;

	// Searching for beginning and end points
	Point ptBegin, ptEnd;
	initTime = clock();

	if (getBeginEnd(imgLabyrinth, ptBegin, ptEnd) == EXIT_FAILURE) {
		cout << "The labyrinth lacks a beginning or/and an end." << std::endl;
		return EXIT_FAILURE;
	}

	cout << "Begin/end finding time: " << clock() - initTime << " ms" << endl;
	
	// Searching for path between begin and end point
	std::vector<Point> tabPoints;
	initTime = clock();

	if (getShortestPath(imgLabyrinth, ptBegin, ptEnd, stoi(argv[2]), stoi(argv[3]), tabPoints, imgResult) == EXIT_FAILURE) {
		cout << "There is not path from the beginning to the end." << endl;
		waitKey(0);
		return EXIT_FAILURE;
	}

	cout << "Processing time: " << clock() - initTime << " ms" << endl;

	imshow("Display window2", imgResult);
	waitKey(1); // waiting at least 1ms (else nothing is displayed)

	// Writing result image on disk
	initTime = clock();
	imwrite(argv[4], imgResult);
	cout << "Result writing time: " << clock() - initTime << " ms" << endl;

	//resize(imgLabyrinth, imgLabyrinthZoom, Size(), 5, 5, 0);
	//namedWindow("Display window", WINDOW_AUTOSIZE); // Create a window for display.
	//imshow("Display window", imgLabyrinthZoom); // Show our image inside it.
	waitKey(0);

	return EXIT_SUCCESS;
}

