#pragma once

#include "stdafx.h"
#include "SP_Pixel.h"

#include <queue>

int getBeginEnd(Mat& inImg, Point& outPtBegin, Point& outPtEnd);
int getShortestPath(Mat& inImg, Point& inPtBegin, Point& inPtEnd, int inSpeedProcess, int inSpeedPath, std::vector<Point>& outTabPts, Mat& outImg);
int addNeighbourPixel(SP_Pixel& inNeighPix, SP_Pixel& inCurPix, Point& inPtEnd, priority_queue<SP_Pixel>& inPixHeap, Mat& inImg, Mat& inImgProcessed, std::vector<SP_Pixel>& inOutTabPts);
int getLabyrinthPath(std::vector<SP_Pixel>& inTabPts, Point& inPtEnd, Mat& inImg, std::vector<Point>& outTabPts);
int printPathInImage(std::vector<Point>& inTabPts, Mat& inImg, Vec3b inColor, int inSpeed);
bool isNeighbourOK(SP_Pixel& inNeighPix, Mat& inImg, Mat& inImgProcessed);
bool getOnlyPossibleNeighbour(SP_Pixel& inCurPix, Mat& inImg, Mat& inImgProcessed, SP_Pixel& outNeighbour);
